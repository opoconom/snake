var canvas = document.querySelector('canvas');
var size = 20;

canvas.width = size * 30;
canvas.height = size * 30;

var context = canvas.getContext('2d'),
    speed = 5,
    oldSpeed = 5,
    accelerates = false,
    apple = {},
    historyOfDirections = [{'x': 1, 'y': 0}],
    step = canvas.width / size;

let snake;

var last = performance.now(),
    dt = 0,
    now,
    id;

class Snake {
    constructor(size, x, y, color) {
        this.size = size;
        this.color = color;
        this.body = [{'directionX': 1, 'directionY': 0, 'x': x, 'y': y}];
    }

    move() {
        this.clear();
        this.updateCoordinates();
    }

    updateCoordinates() {
        var story = historyOfDirections.shift();

        if (story) {
            snake.body[0].directionX = story.x;
            snake.body[0].directionY = story.y;
        }

        var head = {'directionX': this.body[0].directionX, 'directionY': this.body[0].directionY};
        var changeDirection = false;

        if (this.body[0].y === 0 && this.body[0].directionY === -1) {
            head.x = this.body[0].x;
            head.y = canvas.height - this.size;
            changeDirection = true;
        }
        if (this.body[0].y + this.size === canvas.height && this.body[0].directionY === 1) {
            head.x = this.body[0].x;
            head.y = 0;
            changeDirection = true;
        }
        if (this.body[0].x === 0 && this.body[0].directionX === -1) {
            head.x = canvas.width - this.size;
            head.y = this.body[0].y;
            changeDirection = true;
        }
        if (this.body[0].x + this.size === canvas.width && this.body[0].directionX === 1) {
            head.x = 0;
            head.y = this.body[0].y;
            changeDirection = true;
        }
        if (!changeDirection) {
            head.x = this.body[0].x + (this.size * this.body[0].directionX);
            head.y = this.body[0].y + (this.size * this.body[0].directionY);
        }

        this.body.unshift(head);
        this.body.pop();
    }

    plusPlus(element) {
        this.body.push({
            'directionX': element.directionX,
            'directionY': element.directionY,
            'x': element.x,
            'y': element.y
        });
    }

    clear() {
        var size = this.size;
        this.body.forEach(function (element) {
            context.clearRect(element.x, element.y, size, size);
        });
    }

    draw(body = this.body, size = this.size, color = this.color) {
        context.beginPath();

        var body = JSON.parse(JSON.stringify(body)),
            head = body.shift();

        context.fillStyle = '#000';
        context.fillRect(head.x, head.y, size, size);

        var strip = true;

        body.forEach(function (element) {
            if (strip) {
                if (!accelerates) {
                    context.fillStyle = '#00900D';
                } else {
                    context.fillStyle = '#DF0101';
                }
            } else {
                if (!accelerates) {
                    context.fillStyle = '#006309';
                } else {
                    context.fillStyle = '#8A0808';
                }
            }

            strip = !strip;

            context.fillRect(element.x, element.y, size, size);
        });

        context.save();
        context.restore();
    }

    checkSelfCollision() {
        var body = (JSON.parse(JSON.stringify(this.body))),
            head = body.shift(),
            collision = false;

        body.forEach(function (element) {
            if (element.x === head.x && element.y === head.y) {
                collision = true;
            }
        });

        return collision;
    }
}

class Apple {

    constructor(size) {
        this.size = size;
        this.x = 0;
        this.y = 0;
        this.randomCoordinates();
        this.draw();
    }

    randomCoordinates() {
        this.x = Math.floor(Math.random() * step) * snake.size;
        this.y = Math.floor(Math.random() * step) * snake.size;

        var exists = false,
            x = this.x,
            y = this.y;

        snake.body.forEach(function (element) {
            if (element.x === x && element.y === y) {
                exists = true;
            }
        });

        if (exists) {
            this.randomCoordinates();
        }
    }

    draw() {
        var grd = context.createLinearGradient(this.x, this.y, this.x + size, this.y + size);
        grd.addColorStop(0, '#FF5733');
        grd.addColorStop(0.4, '#C70039');
        grd.addColorStop(0.6, '#900C3F');
        grd.addColorStop(1, '#581845');
        context.fillStyle = grd;
        context.fillRect(this.x, this.y, size, size);
    }

    destroy() {
        context.clearRect(this.x, this.y, size, size);
    }
}

var drawApple = () => {
    apple = new Apple(size);
};

var iteration = () => {
    var element = snake.body[snake.body.length - 1];
    snake.move();
    if (snake.body[0].x === apple.x && snake.body[0].y === apple.y) {
        apple.destroy();
        drawApple();

        speed += 1;

        if (snake.body.length < 10) {
            speed = 7;
            if (accelerates) {
                speed *= 2;
            }
            oldSpeed = 7;
        } else if (snake.body.length < 20) {
            speed = 10;
            if (accelerates) {
                speed *= 2;
            }
            oldSpeed = 10;
        } else if (snake.body.length < 30) {
            speed = 15;
            if (accelerates) {
                speed *= 2;
            }
            oldSpeed = 15;
        } else {
            speed = 20;
            if (accelerates) {
                speed *= 2;
            }
            oldSpeed = 20;
        }

        snake.plusPlus(element);
        updateScore();
    }
};

var addHistoryOfDirections = (direction) => {
    if (historyOfDirections.length < 2) {
        historyOfDirections.push(direction);
    }
};

var updateScore = () => {
    document.getElementById('score').innerHTML = snake.body.length - 1;
    document.getElementById('speed').innerHTML = speed;
};

document.onkeydown = function (event) {

    keyCode = window.event.keyCode;
    keyCode = event.keyCode;

    switch (keyCode) {
        case 37:
            if (snake.body[0].directionX != 1) {
                addHistoryOfDirections({'x': -1, 'y': 0});
            }
            break;
        case 39:
            if (snake.body[0].directionX != -1) {
                addHistoryOfDirections({'x': 1, 'y': 0});
            }
            break;
        case 38:
            if (snake.body[0].directionY != 1) {
                addHistoryOfDirections({'x': 0, 'y': -1});
            }
            break;
        case 40:
            if (snake.body[0].directionY != -1) {
                addHistoryOfDirections({'x': 0, 'y': 1});
            }
            break;
        case 32:
            if (!accelerates) {
                oldSpeed = speed;
                speed = speed * 2;
                accelerates = true;
                updateScore();
            }
            break;
    }
};

document.onkeyup = function accelerate(event) {
    keyCode = window.event.keyCode;
    keyCode = event.keyCode;

    if (keyCode === 32 && accelerates) {
        speed = oldSpeed;
        accelerates = false;
        updateScore();
    }
};

var frame = () => {
        var step = 1 / speed,
        prevBody = {};

        now = performance.now();
        dt += (now - last) / 1000;

        while (dt > step) {
            dt = dt - step;
            prevBody = JSON.parse(JSON.stringify(snake.body));
            iteration();

            if (snake.checkSelfCollision()) {
                cancelAnimationFrame(id);
                snake.draw(prevBody);
                return loose();
            }
        }

        snake.draw();
        last = now;
        id = requestAnimationFrame(frame);
};

function run()
{
    speed = 5,
    oldSpeed = 5,
    accelerates = false,
    apple = {},
    historyOfDirections = [{'x': 1, 'y': 0}],
    step = canvas.width / size;
    last = performance.now();

    snake = new Snake(size, size, size, '#AB95DE');
    context.clearRect(0, 0, canvas.width, canvas.height);   
 
    frame();
    drawApple();
    updateScore();
}

function loose()
{
    alert(snake.body.length - 1);
    run();
}